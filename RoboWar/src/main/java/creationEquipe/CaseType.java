package creationEquipe;
/**
 *Défint le contenu d'une case d'une carte
 *le contenu est soit: un robot, un mur , vide , ou un drapeau
 *
 */

public enum CaseType {
	VID, MUR, ROB, DRAPEAU;
	
}