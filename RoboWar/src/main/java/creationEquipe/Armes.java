package creationEquipe;
/**
 * Définit les armes utiliser par un robot pour attaquer un autre robot
 *
 */

public class Armes {
	/**
	 * le nom de l'arme
	 */
	private String nom;
	/**
	 * le nombre de cooup de l'arme
	 */
	private int nbrCoup;
	/**
	 * la portée de l'arme
	 */
	private int portee;
	/**
	 * le monbre de point d'équipement nécessaire pour la creation de cette arme
	 */
	private int NbrPoindEquipement;
	

	/**
	 * instanciation d'une arme
	 * @param nom de l'arme
	 * @param nombre de coup
	 * @param portee de l'arme
	 */
	public Armes(String nom, int nbrCoup, int portee) {
		this.nom = nom;
		this.nbrCoup = nbrCoup;
		this.portee = portee;
		this.NbrPoindEquipement = nbrCoup * 5+ portee * 7;

	}

	public String getNom() {
		return this.nom;
	}

	// Suprimer le coup urilisé
	public void supprimerCoup() {
		this.nbrCoup--;

	}
	
	public int getNbrCoup() {
		return nbrCoup;
	}

	public int getPortee() {
		return portee;
	}

	public int getNbrPoindEquipement() {
		return NbrPoindEquipement;
	}	
	

}
