package creationEquipe;
/**
 * Définit le type d'action qu'un robot peux effectuer
 *
 */

public class Action {
	/**
	 * enum des action d'un robot : DETECT,DEPLACE,ATTAQUE
	 *
	 */
	public enum CodeAction {
		DETECT, DEPLACE, ATTAQUE
	};
	
	/**
	 * code le l'action a effectuer par le robot
	 */
	private CodeAction codeAction;
	/**
	 * la position du deplacement du robot si le code d'action est DEPLACE
	 * la position du robot a attaquer si le code de l'action est ATTAQUE
	 */
	private Position position;
	
	public CodeAction getCodeAction() {
		return codeAction;
	}
	public void setCodeAction(CodeAction codeAction) {
		this.codeAction = codeAction;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
	/**
	 * initialise le code de l'action a detecter
	 * et la position a (-1,-1)
	 */
	public Action() {
		this.codeAction = CodeAction.DETECT;
		this.position = new Position(-1,-1);
		}

}
