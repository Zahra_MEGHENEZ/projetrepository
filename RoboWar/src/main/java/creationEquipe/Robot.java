/*
 * Class Robot
 *
 * 1.0
 *
 * 21/10/2012
 *
 * copyright: M2 ACSIS 2012/2013
 */

package creationEquipe;


/**
 * Définit la Structure d'un Robot
 * 
 */
public  class Robot {
	/**
	 * la position du robot dans la carte du jeux
	 */
	private Position positionRobot;
	/**
	 * identifiacteur du robot
	 */
	private String idRobot;
	/**
	 * equipe du robot
	 */
	private String equipe;
	
	/**
	 * l'arme utilise par le robot
	 */
	private Armes arme;
	/**
	 * le nombre de point d'équipement nécessaire pour la creation d'un robot
	 */
	protected int NbrPoindEquipement;

	/**
	 * initialiser un robot en lui donnant des parametre
	 * @param id
	 * @param equipe
	 * @param arme
	 */
	public Robot(String id, String equipe,  Armes arme) {
		this.idRobot = id;
		this.equipe = equipe;
		this.arme = arme;
		this.NbrPoindEquipement = 10 + arme.getNbrPoindEquipement();
		this.positionRobot = new Position();
	}
	/**
	 * initlaliser un robot 
	 */
	public Robot() {
		this.idRobot = "";
		this.equipe = "";
		this.positionRobot = new Position();
	}

	public Position getPositionRobot() {
		return positionRobot;
	}

	public void setPositionRobot(Position positionRobot) {
		//System.out.println(this.getPositionRobot().getPositionX()+"      "+this.getPositionRobot().getPositionY());
		this.positionRobot = positionRobot;
		//System.out.println(this.getPositionRobot().getPositionX()+"      "+this.getPositionRobot().getPositionY());
		
	
	}

	public String getIdRobot() {
		return idRobot;
	}

	public String getEquipe() {
		return equipe;
	}
	
	public void setEquipe(String nomEquipe) {
		this.equipe = nomEquipe;
	}
	public Armes getArme() {
		return arme;
	}

	public int getNbrPoindEquipement() {
		return NbrPoindEquipement;
	}


}
