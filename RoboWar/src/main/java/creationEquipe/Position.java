package creationEquipe;

import java.io.Serializable;



/**
 * Définit les cordonnées (X,Y) dans la carte du jeux 
 *
 */
public class Position implements Serializable {
	/**
	 * position de la ligne
	 */
	private int positionX;
	/**
	 * position de la colonne
	 */
	private int positionY;
	
	/**
	 * initialiser une position a x,y
	 * @param x
	 * @param y
	 */
	public Position(int x, int y) {
		this.positionX = x;
		this.positionY = y;
	}
	/**
	 * initialise les position a -1,-1
	 */
	public Position() {
		this.positionX = -1;
		this.positionY = -1;
	}
	
	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}
	public Position clone() {
		Position p2 = new Position();
		p2.positionX = this.positionX;
		p2.positionY = this.positionY;
		return p2;
	}

}
