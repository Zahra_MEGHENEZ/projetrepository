package creationEquipe;

import creationEquipe.Action.CodeAction;


/**
 *  définit une équipe appelé equipe1 elle hérite de la classe Equipe
 * la stratégie de cette équipe est d'attaquer 
 *
 */
public class Equipe1 extends Equipe {

	boolean conditionDeVictoire[] = { true, true, true, true };

	// LE CONSTRUCTEUR POUR MON EQUIPE Equipe1
	/**
	 * instancie l'équipe1 , on lui donne  un nom et on insére   des robots
	 */
	public Equipe1() {
		this.nomEquipe = "equipe1";
		int i = 0; // une variable pour changer le nom des robots
		String nomRobot = "robot" + i;
		boolean ajout = false;// la variable qui va contenir le retour de
								// addRobot
		do {
			// AJOUT D'UN ROBOT TOUT EN LE CRÉANT AVEC L'ARME
			ajout = this.addRobot(new Robot(nomRobot, this.nomEquipe,
					new Armes("monarme", 2, 1)));
			// DEFINIR LE NOM DU PROCHAIN ROBOT
			i++;
			nomRobot = "robot" + i;
		} while (ajout);

	}

	
	
	public Robot choixRobotJoue() {	
		return (Robot) getListRobots().get((int)Math.random() * (3) + 0);
	}
  
	public Action choixAction(Robot robot) {
		Action action = new Action();
		int dep = -1;
		if (this.infosDetectees[0] != null) {
			if (this.infosDetectees[0].getContenu() == CaseType.VID) {
				dep = 1;
			}
			if (this.infosDetectees[0].getContenu() == CaseType.ROB) {
				dep = 2;
			}
			if ((this.infosDetectees[0].getContenu() == CaseType.MUR)
					|| (this.infosDetectees[0].getContenu() == CaseType.DRAPEAU)) {
				dep = 10;
			}
		}
		if (dep == 10) {
			if (this.infosDetectees[1].getContenu() == CaseType.VID) {
				dep = 3;
			}
			if (this.infosDetectees[1].getContenu() == CaseType.ROB) {
				dep = 4;
			}
			if ((this.infosDetectees[1].getContenu() == CaseType.MUR)
					|| (this.infosDetectees[1].getContenu() == CaseType.DRAPEAU)) {
				dep = 11;
			}
		}
		if (dep == 11) {
			if (this.infosDetectees[2].getContenu() == CaseType.VID) {
				dep = 5;
			}
			if (this.infosDetectees[2].getContenu() == CaseType.ROB) {
				dep = 6;
			}
			if ((this.infosDetectees[2].getContenu() == CaseType.MUR)
					|| (this.infosDetectees[2].getContenu() == CaseType.DRAPEAU)) {
				dep = 12;
			}
		}
		if (dep == 12) {
			if (this.infosDetectees[3].getContenu() == CaseType.VID) {
				dep = 7;
			}
			if (this.infosDetectees[3].getContenu() == CaseType.ROB) {
				dep = 8;
			}
			// if (this.infosDetectees[3].getContenu()==CaseType.DRAPEAU)
			// {dep=9;}
		}
		// ///////////////////////retourner action et position suivant la valeur
		// de dep
		switch (dep) {
		case 1:
			action.setCodeAction(CodeAction.DEPLACE);
			action.setPosition(new Position(this.infosDetectees[0]
					.getPosition().getPositionX(), this.infosDetectees[0]
					.getPosition().getPositionY()));

			break;
		case 3:
			action.setCodeAction(CodeAction.DEPLACE);
			action.setPosition(new Position(this.infosDetectees[1]
					.getPosition().getPositionX(), this.infosDetectees[1]
					.getPosition().getPositionY()));
			break;
		case 5:
			action.setCodeAction(CodeAction.DEPLACE);
			action.setPosition(new Position(this.infosDetectees[2]
					.getPosition().getPositionX(), this.infosDetectees[2]
					.getPosition().getPositionY()));
			break;
		case 7:
			action.setCodeAction(CodeAction.DEPLACE);
			action.setPosition(new Position(this.infosDetectees[3]
					.getPosition().getPositionX(), this.infosDetectees[3]
					.getPosition().getPositionY()));
			break;
		case 2:
			action.setCodeAction(CodeAction.ATTAQUE);
			action.setPosition(new Position(this.infosDetectees[0]
					.getPosition().getPositionX(), this.infosDetectees[0]
					.getPosition().getPositionY()));
			break;
		case 4:
			action.setCodeAction(CodeAction.ATTAQUE);
			action.setPosition(new Position(this.infosDetectees[1]
					.getPosition().getPositionX(), this.infosDetectees[1]
					.getPosition().getPositionY()));
			break;
		case 6:
			action.setCodeAction(CodeAction.ATTAQUE);
			action.setPosition(new Position(this.infosDetectees[2]
					.getPosition().getPositionX(), this.infosDetectees[2]
					.getPosition().getPositionY()));
			break;
		case 8:
			action.setCodeAction(CodeAction.ATTAQUE);
			action.setPosition(new Position(this.infosDetectees[3]
					.getPosition().getPositionX(), this.infosDetectees[3]
					.getPosition().getPositionY()));
			break;
		}

		return action;
	}

}
