package creationEquipe;

import java.util.Iterator;

import creationEquipe.Action.CodeAction;

/**
 * définie  une équipe appelée équipe2
 *La stratégie des robot de cette équipe est d'esciver des murs 
 *et des robots des équipes adverses et de se déplacer
 *
 */
public class Equipe2 extends Equipe {

	/**
	 * robot 1 de l'équipe2
	 */
	public Robot r1 = new Robot();
	/**
	 *  robot 2 de l'équipe2
	 */
	public Robot r2 = new Robot();
	/**
	 *  robot 3 de l'équipe2
	 */
	public Robot r3 = new Robot();
	
	/**
	 * utiliser pour choisir le robot qui joue
	 */
	int choix = 1;
	/**
	 * la direction de depalcement
	 */
	int direct;

	int v = 1;

	public Equipe2() {

		this.nomEquipe = "e2";
		this.direct = 0;

		Armes ar1 = new Armes("fusee", 0, 0);

		// CREATION DES ROBOT

		r1 = new Robot("r21", this.nomEquipe, ar1);
		this.addRobot(r1);

		r2 = new Robot("r22", this.nomEquipe, ar1);
		addRobot(r2);

		r3 = new Robot("r23", this.nomEquipe, ar1);
		this.addRobot(r3);

	}

	public Robot choixRobotJoue() {
		// tEST SI LE ROBOT EXISTE

		this.choix = 1;// réinitialiser
		Robot r = null;
		Iterator<Robot> iter = getListRobots().iterator();
		if (this.v == 1) {
			while (iter.hasNext()) {
				if (iter.next().getIdRobot() == "r21") {

					r = this.r1;

				}

			}
			this.v = 2;
		} else {
			if (this.v == 2) {
				while (iter.hasNext()) {
					if (iter.next().getIdRobot() == "r22") {

						r = this.r2;
					}
				}
				this.v = 3;
			} else {
				if (this.v == 3) {
					while (iter.hasNext()) {
						if (iter.next().getIdRobot() == "r23") {
							this.v = 1;

						}
					}
					r = this.r3;
				}
			}
		}
		return r;
	}

	public Action choixAction(Robot robot) {
		boolean trouver = true;
		Action info = new Action();
		if (direct == 0) {
			if (robot.getPositionRobot().getPositionX() >= 10)
				direct = 1;
			else
				direct = 2;
		}
		String dir = "";
		if (this.choix == 1) {
			// info[1]=1; //detecter
			info.setCodeAction(CodeAction.DETECT);
			this.choix = 2;
		} else {

			info.setCodeAction(CodeAction.DEPLACE);

			// choisir la case ou se deplacer en fonction du retour de la
			// detection

			while (trouver) {
				if (direct == 1) {

					if ((infosDetectees[0] != null)
							&& ((infosDetectees[0].getContenu()
									.equals(CaseType.VID)) || (infosDetectees[0]
									.getContenu().equals(CaseType.DRAPEAU)))) {
						info.setPosition(infosDetectees[0].getPosition());
						dir = "g";
						trouver = false;
					} else {
						if ((infosDetectees[1] != null)
								&&((infosDetectees[1].getContenu()
										.equals(CaseType.VID)) || (infosDetectees[1]
												.getContenu().equals(CaseType.DRAPEAU)))) {
							info.setPosition(infosDetectees[1].getPosition());
							dir = "h";
							trouver = false;
						} else {
							if ((infosDetectees[2] != null)
									&& ((infosDetectees[2].getContenu()
											.equals(CaseType.VID)) || (infosDetectees[2]
													.getContenu().equals(CaseType.DRAPEAU)))) {
								info.setPosition(infosDetectees[2]
										.getPosition());
								dir = "d";
								trouver = false;
							} else {
								if ((infosDetectees[3] != null)
										&& ((infosDetectees[3].getContenu()
												.equals(CaseType.VID)) || (infosDetectees[3]
														.getContenu().equals(CaseType.DRAPEAU)))) {
									info.setPosition(infosDetectees[3]
											.getPosition());
									dir = "b";
									trouver = false;
								} else // rester dans la mm case (pas de
										// deplacement)
								{
									info.setPosition(robot.getPositionRobot());
									trouver = false;
								}

							}

						}
					}

				} else {
					if ((infosDetectees[2] != null)
							&& ((infosDetectees[2].getContenu()
									.equals(CaseType.VID)) || (infosDetectees[2]
									.getContenu().equals(CaseType.DRAPEAU)))) {
						info.setPosition(infosDetectees[2].getPosition());
						trouver = false;
					} else {
						if ((infosDetectees[3] != null)
								&& ((infosDetectees[3].getContenu()
										.equals(CaseType.VID)) || (infosDetectees[3]
												.getContenu().equals(CaseType.DRAPEAU)))) {
							info.setPosition(infosDetectees[3].getPosition());
							trouver = false;
						} else {
							if ((infosDetectees[1] != null)
									&& ((infosDetectees[1].getContenu()
											.equals(CaseType.VID)) || (infosDetectees[1]
													.getContenu().equals(CaseType.DRAPEAU)))) {
								info.setPosition(infosDetectees[1]
										.getPosition());
								trouver = false;
							} else {
								if ((infosDetectees[0] != null)
										&& ((infosDetectees[0].getContenu()
												.equals(CaseType.VID)) || (infosDetectees[0]
														.getContenu().equals(CaseType.DRAPEAU)))) {
									info.setPosition(infosDetectees[0]
											.getPosition());
									trouver = false;
								} else // rester dans la mm case (pas de
										// deplacement)
								{
									info.setPosition(robot.getPositionRobot());
									trouver = false;
								}

							}

						}
					}

				}

				choix = 1;
			}
		}
		return info;

	}

	// instancier l'equipe2 (equipe former de 3 robot )
	private Robot getRobot(String idRobot) {
		boolean trouve = false;
		Robot robot = new Robot();
		Iterator<Robot> it = this.getListRobots().iterator();
		while (it.hasNext() && !trouve) {
			robot = it.next();
			if (robot.getIdRobot().equals(idRobot))
				trouve = true;

		}
		return robot;

	}

}