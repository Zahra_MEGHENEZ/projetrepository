package creationEquipe;

import java.io.Serializable;
/**
 * 
 *Définit les cases d'une carte 
 * 
 */

public class Case implements Serializable {

	// DEFINITION DES VARIABLE DE CLASSE
	/**
	 * les cordonnees de la case(X,Y)
	 */
	private Position position;
	/**
	 * contenu soit robot, vide , drapeau, ou zone de depart
	 */
	private CaseType contenu;
	/**
	 *  si contenu est Zone de dpart c'est nom de la zone de depart
	 */
	private String zoneDepart;
	/**
	 * si le contenu est robot nom du robot 
	 */
	private String idRobot;
	/**
	 * nom de l'equipe d'un robot ou de l'equipe placer dans la zone de départ
	 */
	private String nomEquipe;
	/**
	 * chaque contenu aun symbole bien spécifique
	 */
	private String symbole;

	public String getSymbole() {
		return symbole;
	}

	public void setSymbole(String symbole) {
		this.symbole = symbole;
	}

	// DEFINITION DES CONSTRUCTEURS
	/**
	 * initialise une case  a vide  en lui fournissant sa position
	 * @param pososition 
	 */
	public Case(Position pos) {
		this.contenu = CaseType.VID;
		this.zoneDepart = "";
		this.idRobot = "";
		this.nomEquipe = "";
		this.setSymbole("-");
		this.position = pos;
	}

	/**
	 * initialise une case en lui fourissant des parametres 
	 * @param contenu
	 * @param zone de Depart
	 * @param idRobot
	 * @param nomEquipe
	 * @param position
	 */
	public Case(CaseType contenuC, String zoneDepartC, String idRobotC,
			String nomEquipeC, Position positionC) {
		this.setPosition(positionC);
		contenu = contenuC;
		zoneDepart = zoneDepartC;
		idRobot = idRobotC;
		nomEquipe = nomEquipeC;
	}

	// LES ACCESSEURS
	public String getNomEquipe() {
		return nomEquipe;
	}

	public void setNomEquipe(String nomEquipeA) {
		this.nomEquipe = nomEquipeA;
	}

	public String getIdRobot() {
		return idRobot;
	}

	public void setIdRobot(String nomRobotA) {
		idRobot = nomRobotA;
	}

	public CaseType getContenu() {
		return contenu;
	}

	public void setContenu(CaseType contenuC) {
		this.contenu = contenuC;
	}

	public String getZoneDepart() {
		return zoneDepart;
	}

	public void setZoneDepart(String zoneDepartC) {
		zoneDepart = zoneDepartC;
	}

	public Position getPosition() {
		return this.position;
	}

	public void setPosition(Position positionC) {
		this.position.setPositionX(positionC.getPositionX());
		this.position.setPositionY(positionC.getPositionY());
	}
}