package creationEquipe;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * définit la structure d'une équipe.
 *
 */

public abstract class Equipe {

	/**
	 * le nom de l'équipe
	 */
	protected String nomEquipe;
	/**
	 * la liste de robot de l'équipe
	 */
	private  ArrayList<Robot> listRobots = new ArrayList<Robot>(); 
	/**
	 * information detecter 
	 */
	public  Case infosDetectees[]=new Case[4];
	
	
	public void setNomEquipe(String nomEquipe) {
		this.nomEquipe = nomEquipe;
	}

	public String getNomEquipe(){
		return this.nomEquipe;
	}
	
	public ArrayList<Robot> getListRobots(){
		return this.listRobots;
	}
	public final void deleteRobot(Robot robot) {

		int index = this.listRobots.indexOf(robot);
		this.listRobots.remove(index);

		}
			/**
			 * Cette methode permet d'ajouter un robot à la liste des robots de
			 * l'equipe, si le nombre de points d'equipement n'est pas depassé
			 * le nombre de points d'equipement est supposé 100.
			 * @param robot
			 * @return si le robot est ajouter ell retourne true sinon false
			 */
			public final boolean addRobot(Robot robot) {
		
		int nbrPoitEquipementUtilisee = 0;
		Iterator<Robot> it = this.listRobots.iterator();
		while (it.hasNext()) {

		nbrPoitEquipementUtilisee += it.next().getNbrPoindEquipement();
		}
		if ((nbrPoitEquipementUtilisee + robot.getNbrPoindEquipement()) <= 100) { 
		// 100 est le nombre de points d'action à ne pas depasser
		this.listRobots.add(robot);
		return true;
		} else
		System.out.println("vous avez epuisé le nombre de poind d'equipement autorisé");
		return false;
		}
	/**
	 * choisir un robot pour jouer 
	 * @return une robot
	 */
	public abstract Robot choixRobotJoue();
	/**
	 * choix de l'action a effectuer par un robot
	 * @param robot
	 * @return action (le code de l'action et une position)
	 */
	public abstract Action choixAction(Robot robot);
	


}
