package moteurJeux;

import java.awt.Image;
import java.util.HashMap;

import javax.swing.ImageIcon;

public class Incones {

	HashMap< String, ImageIcon> inconesImage;

	public Incones() {
		inconesImage = new HashMap<String, ImageIcon>();
		ImageIcon mur = new ImageIcon(new ImageIcon(
				"images\\mur.jpg").getImage().getScaledInstance(30,
				30, Image.SCALE_DEFAULT));
		inconesImage.put("#", mur);
		
		ImageIcon drapeau = new ImageIcon(new ImageIcon(
				"images\\drapeau.png").getImage().getScaledInstance(30,
				30, Image.SCALE_DEFAULT));
		inconesImage.put("D", drapeau);
		
		ImageIcon vide = new ImageIcon(new ImageIcon(
				"images\\vid.png").getImage().getScaledInstance(50,
				50, Image.SCALE_DEFAULT));
		inconesImage.put("-",  vide);
		
		ImageIcon zoneDepart = new ImageIcon(new ImageIcon(
				"images\\zonedepart.png").getImage().getScaledInstance(50,
				50, Image.SCALE_DEFAULT));
		inconesImage.put("*",  zoneDepart);
		
		ImageIcon robEquipe1 = new ImageIcon(new ImageIcon(
				"images\\robot.png").getImage().getScaledInstance(50,
				50, Image.SCALE_DEFAULT));
		inconesImage.put("&",  robEquipe1);
		
		
		ImageIcon  robEquipe2 = new ImageIcon(new ImageIcon(
				"D:\\java\\robot.png").getImage().getScaledInstance(50,
				50, Image.SCALE_DEFAULT));
		inconesImage.put("~",  robEquipe2);
		
		ImageIcon  robEquipe3 = new ImageIcon(new ImageIcon(
				"D:\\java\\robot.png").getImage().getScaledInstance(50,
				50, Image.SCALE_DEFAULT));
		inconesImage.put("^",  robEquipe3);
		
		ImageIcon  robEquipe4 = new ImageIcon(new ImageIcon(
				"D:\\java\\robot.png").getImage().getScaledInstance(50,
				50, Image.SCALE_DEFAULT));
		inconesImage.put("_",  robEquipe4);
		
	}
	
	
	

}
