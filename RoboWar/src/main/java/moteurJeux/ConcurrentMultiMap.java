package moteurJeux;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

/**
 * Prend en charge le gestion d'acces concurent a une map
 * 
 * @param <K>
 *            cles d'acces
 * 
 * @param <V>
 *            les valeurs (c'est un Set , mais on met a l'interieur un element a
 *            la fois)
 * 
 */
public class ConcurrentMultiMap<K extends Comparable, V> {
	private final ConcurrentMap<K, Set<V>> cache = Maps.newConcurrentMap();

	public void put(final K key, V value) {
		Set<V> set = cache.get(key);

		if (set != null)

			cache.remove(key);
		set = Sets.newSetFromMap(Maps.<V, Boolean> newConcurrentMap());
		final Set<V> previousSet = cache.putIfAbsent(key, set);
		if (previousSet != null) {
			set = previousSet;
		}
		set.add(value);

	}

	public V get(K key) {

		Set<V> set = cache.get(key);
		Iterator<V> it = set.iterator();
		return it.next();

	}

	public Set<V> remove(K key) {
		return cache.remove(key);
	}

	public Set<K> getKeySet() {
		return cache.keySet();
	}

	public int size() {
		return cache.size();
	}

}
