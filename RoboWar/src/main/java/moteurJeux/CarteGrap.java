package moteurJeux;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import cartes.*;

public class CarteGrap extends JFrame {
	Carte carte;
	int x, y;
	Incones mesincone;
	JPanel pan,pan1;
	
	public CarteGrap(Carte carte) {
        this.carte = carte;
		this.setTitle("La CARTE");
		this.setSize(800, 640);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		mesincone = new Incones();
		pan = new JPanel();
		pan1 = new JPanel();
		
		
		x = carte.getDimensionX();
		y = carte.getDimensionY();
		pan.setLayout(new GridLayout(x,y));
		pan.setLayout(new GridLayout(x+1,y,2,2));
		pan.setBackground(Color.cyan);
		
		pan1.setLayout(new GridLayout(1,1,2,2));
		pan1.setBackground(Color.BLUE);
		JButton  valide = new JButton();
		pan1.add(valide);
		pan1.setVisible(true);
		
		affiche(pan);
		
		this.setContentPane(pan);
		this.setVisible(true);

	}
	public  void affiche(JPanel pan){
		 
		 for (int i = 0; i < x; i++) {
				//Border border = BorderFactory.createLineBorder(Color.blue);
				for (int j = 0; j < y; j++) {
		            String symbole = carte.getCarte()[i][j].getSymbole();
					JLabel label = new JLabel(mesincone.inconesImage.get(symbole));
					pan.add(label);
		        
				}
			}
		
		
	}
	public  void reaffiche(){
		pan.removeAll();
		affiche(pan);
		pan.add(pan1);
		this.setContentPane(pan);
	}
	
	public static void main(String[] args) {
		Carte carte = new Carte1("Lacarte");
		new CarteGrap(carte);
	}
	

}