package moteurJeux;

import java.util.HashMap;
/**
 * On l'associe à chaque equipe qui joue, il nous permet d'Enregistrer pour
 * chaque robots d'une equipe les cases qu'il a parcourut
 */
public class ParcourtCarte {
	

	private HashMap<String, boolean[][]> mapRobotParcours;

	public ParcourtCarte() {
		
		this.mapRobotParcours = new HashMap<String, boolean[][]>();

	}

	public HashMap<String, boolean[][]> getMapRobotParcours() {
		return mapRobotParcours;
	}

	public void setListeDesRobotEquipe(
			HashMap<String, boolean[][]> mapRobotParcours) {
		this.mapRobotParcours = mapRobotParcours;
	}

	public void ajouterRobot(String idRobot, boolean[][] etatDesCase) {
		mapRobotParcours.put(idRobot, etatDesCase);
	}

	public void deteteRobot(String idRobot) {
		mapRobotParcours.remove(idRobot);
	}

}
