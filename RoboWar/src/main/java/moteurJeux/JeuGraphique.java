package moteurJeux;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cartes.*;
import creationEquipe.*;
import creationEquipe.Action.CodeAction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class JeuGraphique {

	final Logger logger = LoggerFactory.getLogger(JeuGraphique.class);
	private InterfaceGraphique fenetre;
	private Carte carteJeux;
	private int nbrToursEffectues;
	private final int pointsAction = 5;
	private int nbrEquipes;
	private boolean existgagnant;
	private String gagant;
	private int nbrTourPourGagner = 50;
	private String equipeJoeu; // la cles de l'equipe qui joue
	private String equipeAttaquee; // la cles de l'equipe attaquée
	private Robot robotJoeu;
	private Action action; // l'action a executer
	private ConcurrentMultiMap<String, Equipe> listEquipes;
	private ConcurrentMultiMap<String, ParcourtCarte> mapDesEquipes;

	// conditions de victoire de chaque equipe
	// private HashMap<String, boolean[]> condVectoireEquipes;
	private int pointActionReste;

	public enum Dir {
		G, D, H, B, M, NP;

	}
	
	public int getLongListeEquipe(){
		return this.listEquipes.size();
	}
	protected void setCarteJeux(Carte carte){
		this.carteJeux = carte;
	}
	public Carte getCarteJeux(){
		return this.carteJeux;
	}



	// le nbr points d'action minimum necessaire pour effectuer une action
	// private int minPointAction =1;

	public Case[] detecter() {

		Position p1 = new Position();
		Position p2 = new Position();
		Position p3 = new Position();
		Position p4 = new Position();
		this.pointActionReste -= 2;

		// case (x,y-1)
		if (robotJoeu.getPositionRobot().getPositionY() != 0) {
			p1.setPositionX(robotJoeu.getPositionRobot().getPositionX());
			p1.setPositionY(robotJoeu.getPositionRobot().getPositionY() - 1);
			listEquipes.get(equipeJoeu).infosDetectees[0] = carteJeux
					.getCase(p1);
		} else {
			listEquipes.get(equipeJoeu).infosDetectees[0] = null;
		}
		// case (x-1,y)
		if (robotJoeu.getPositionRobot().getPositionX() != 0) {
			p2.setPositionX(robotJoeu.getPositionRobot().getPositionX() - 1);
			p2.setPositionY(robotJoeu.getPositionRobot().getPositionY());
			listEquipes.get(equipeJoeu).infosDetectees[1] = carteJeux
					.getCase(p2);
		} else {
			listEquipes.get(equipeJoeu).infosDetectees[1] = null;
		}

		// case (x,y+)

		if (robotJoeu.getPositionRobot().getPositionX() != this.carteJeux
				.getDimensionY() - 1) {
			p3.setPositionX(robotJoeu.getPositionRobot().getPositionX());
			p3.setPositionY(robotJoeu.getPositionRobot().getPositionY() + 1);
			listEquipes.get(equipeJoeu).infosDetectees[2] = carteJeux
					.getCase(p3);
		} else {
			listEquipes.get(equipeJoeu).infosDetectees[2] = null;
		}

		// case (x+1,y)
		if (robotJoeu.getPositionRobot().getPositionX() != this.carteJeux
				.getDimensionX() - 1) {
			p4.setPositionX(robotJoeu.getPositionRobot().getPositionX() + 1);
			p4.setPositionY(robotJoeu.getPositionRobot().getPositionY());
			listEquipes.get(equipeJoeu).infosDetectees[3] = carteJeux
					.getCase(p4);
		} else {
			listEquipes.get(equipeJoeu).infosDetectees[3] = null;
		}

		return listEquipes.get(equipeJoeu).infosDetectees;

	}

	public void attaquer(Position positionRobotadverse) {
		Position position3;

		if ((robotJoeu.getPositionRobot().getPositionX() == positionRobotadverse
				.getPositionX())
				|| robotJoeu.getPositionRobot().getPositionY() == positionRobotadverse
						.getPositionY()) {
			position3 = atteindrePosition(robotJoeu.getPositionRobot(),
					positionRobotadverse, robotJoeu.getArme().getPortee());
			if (carteJeux.getCase(position3).getContenu() == CaseType.ROB) {

				String idRobotAdverse = carteJeux.getCase(position3)
						.getIdRobot();
				this.equipeAttaquee = carteJeux.getCase(position3)
						.getNomEquipe();
				Robot robotAdverse = getRobotEquipe(idRobotAdverse,
						equipeAttaquee);
				System.out.println(robotAdverse.getIdRobot());
				detruireRobot(robotAdverse, this.equipeAttaquee);

			}
		}

		this.pointActionReste -= this.robotJoeu.getArme().getPortee();
	}

	public void deplacer(Position positionDestination) {
		Position position3;
		position3 = nouvellePosition(robotJoeu.getPositionRobot(),
				positionDestination);
		System.out.println("nouvelle position:" + position3.getPositionX()
				+ "     " + position3.getPositionY());
		if (carteJeux.getCase(position3).getContenu() == CaseType.VID) {
			this.carteJeux.viderCase(robotJoeu.getPositionRobot());
			System.out.println(robotJoeu.getIdRobot() + "       "
					+ robotJoeu.getPositionRobot().getPositionX() + "      "
					+ robotJoeu.getPositionRobot().getPositionY());
			// vider la case quil occupe 
			carteJeux.viderCase(robotJoeu.getPositionRobot());
			robotJoeu.setPositionRobot(position3);
			System.out.println(robotJoeu.getIdRobot() + "       "
					+ robotJoeu.getPositionRobot().getPositionX() + "      "
					+ robotJoeu.getPositionRobot().getPositionY());
			carteJeux.PositionnerRobot(robotJoeu);
		}

		else {
			if (carteJeux.getCase(position3).getContenu() == CaseType.DRAPEAU) {
				// traitement selon les objectifs de son equipe
				this.action.setPosition(position3);
				if (!testGagnant()) { // /le robot doit etre mort
					detruireRobot(robotJoeu, equipeJoeu);
					robotJoeu = null;
				}
			} else {

				if (carteJeux.getCase(position3).getContenu() == CaseType.ROB) {
					detruireRobot(robotJoeu, equipeJoeu);
					robotJoeu = null;

				} else {

					if (carteJeux.getCase(position3).getContenu() == CaseType.MUR) {
						detruireRobot(robotJoeu, equipeJoeu);
						robotJoeu = null;
					}
				}
			}
		}
	}

	public void detruireRobot(Robot robot, String KeyEquipe) {
		/*
		 * detruire un robor ==> supresion de toutes les references qui se
		 * pointent vers lui et la liberation de la case qu'il occupe sur la
		 * carteJeux
		 */

		// supression des informations de son parcourt de la carte
		System.out.println("SUPRESSSION D4UN ROBOT");
		System.out.println("dans la position"
				+ robot.getPositionRobot().getPositionX() + ", "
				+ robot.getPositionRobot().getPositionY());

		mapDesEquipes.get(KeyEquipe).deteteRobot(robot.getIdRobot());

		// vider la case q'il occupe dans la cartejeux
		carteJeux.viderCase(robot.getPositionRobot());

		// Supression de robot dans la liste de rebots de son equipe
		// Et aussi les eventuelles copies si elles existent

		int indexRobot = this.listEquipes.get(KeyEquipe).getListRobots()
				.indexOf(robot);
		while (indexRobot != -1) {
			listEquipes.get(KeyEquipe).deleteRobot(robot);
			indexRobot = this.listEquipes.get(KeyEquipe).getListRobots()
					.indexOf(robot);
		}
	}

	public Robot getRobotEquipe(String idRobot, String keyEquipe) {
		ArrayList<Robot> robots = new ArrayList<Robot>();
		robots = this.listEquipes.get(keyEquipe).getListRobots();
		boolean trouve = false;
		Robot robot = new Robot();
		Iterator<Robot> it = robots.iterator();
		while (it.hasNext() && !trouve) {
			robot = it.next();
			if (robot.getIdRobot().equals(idRobot))
				trouve = true;
		}
		return robot;
	}

	public void chargementSenario(int numeroCarte) {
		// @param: le numero de la carte (1: la premiere..)

		ArrayList<Carte> listCartes = new ArrayList<Carte>();
		SerialiserCartes serialiseCartes = new SerialiserCartes();
		try {
			listCartes = serialiseCartes.dessirialiserCartes();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		this.carteJeux = listCartes.get(numeroCarte);

	}

	public void ChargerEquipe(String URL, String nomEquipe)
			throws InstantiationException, IllegalAccessException {
		if(this.listEquipes.size()<carteJeux.getNbrZones()){
		Equipe equipe = null;
		try {
			File file = new File(URL);

			System.out.println("donner le nom de la class");
			BufferedReader entreeClass = new BufferedReader(
					new InputStreamReader(System.in));
			String classToLoad = "creationEquipe." + entreeClass.readLine();

			URL jarUrl = new URL("jar", "", "file:" + file.getAbsolutePath()
					+ "!/");
			URLClassLoader cl = new URLClassLoader(new URL[] { jarUrl },
					JeuGraphique.class.getClassLoader());
			Class<?> loadedClass = cl.loadClass(classToLoad);
			equipe = (Equipe) loadedClass.newInstance();
			System.out.println(equipe.getNomEquipe());
			listEquipes.put(nomEquipe, equipe);

			ParcourtCarte parcourtCarte = new ParcourtCarte();

			Iterator<Robot> iter = equipe.getListRobots().iterator();
			while (iter.hasNext()) {
				parcourtCarte.ajouterRobot(iter.next().getIdRobot(),
						this.carteJeux.getCarteParcourt().clone());
			}
			this.mapDesEquipes.put(nomEquipe, parcourtCarte);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		}

	}

	public boolean testGagnant() {
		/*
		 * Existe gagnant sera vrais si: 1. Soit l'equipe qui joue realise une
		 * de ses conditions de victoire, 2.Si elle pert tous ses rebot (son
		 * derenier rebot entre dans un mur , ou essaye de se postionnner sur
		 * une case qui contien deja un rebot) et qu'il reste qu'une equipe
		 * adverse qui a la condition detruire tous les rebot comme Cond V.
		 * 
		 * @ return : vrai si existe gagnant , false sinon.
		 */

		int i = 0;
		int j = 0;
		boolean parcouru = true;
		boolean equipeVide = true;

		/*
		 * selon la dereniere action effectué,on teste si cette dernier réalise
		 * une condition de victoire
		 */

		// action : deplacement
		if (action.getCodeAction().equals(CodeAction.DEPLACE)) {

			// teste si un le drapeau adverse est trouvé par l'equipe qui joue
			if (carteJeux.getConditionsVectoire().get(equipeJoeu)[1]) {
				if ((carteJeux.getCase(action.getPosition()).getContenu() == CaseType.DRAPEAU)
						&& (carteJeux.getCase(action.getPosition())
								.getNomEquipe() != equipeJoeu))
				// drapeau adeverse capturer
				{
					this.gagant = equipeJoeu;
					existgagnant = true;
				}

			}

			// condition : explorer toute la carte
			/*
			 * if (carteJeux.getConditionsVectoire().get(equipeJoeu)[3]) { //
			 * parcourir la matrice trace parcourt associer au robot qui a //
			 * joue pour verifier s'il a parcouru toute la carte for (i = 0; i <
			 * carteJeux.getDimensionY(); i++) { for (j = 0; j <
			 * carteJeux.getDimensionY(); j++) { if
			 * (!(mapDesEquipes.get(equipeJoeu) .getMapRobotParcours()
			 * .get(robotJoeu.getIdRobot())[i][j])) { parcouru = false; break; }
			 * 
			 * } } existgagnant = parcouru; if (this.existgagnant) gagant =
			 * this.equipeJoeu; }
			 */
			// Test si les rebot de l'equipe qui joue ne sont pas detruit
			if (this.listEquipes.get(equipeJoeu).getListRobots().size() == 0) {
				// s'il reste qu'une equipe adverse qui a des rebots, et que
				// detruite tous les rebot adverse est une condition de vectoire
				// pour celle ci => elle gagne

				this.listEquipes.remove(equipeJoeu);
				System.out.println("je suis passé par la");

				if (this.listEquipes.size() == 1) {
					for (String keyEqui : listEquipes.getKeySet()) {
						if (carteJeux.getConditionsVectoire().get(keyEqui)[1]) {
							existgagnant = true;
							this.gagant = keyEqui;

						}

					}

				}

			}

		}

		/* l'action : attaquer */
		if (action.getCodeAction().equals(CodeAction.ATTAQUE)) {
			/* condition : détruire tous les robot adverse */
			if (carteJeux.getConditionsVectoire().get(equipeJoeu)[2]) {

				for (String key1 : this.listEquipes.getKeySet()) {

					if (key1 != equipeJoeu) {
						if (listEquipes.get(key1).getListRobots().size() != 0) {
							equipeVide = false;
							break;
						}
					}

				}
				existgagnant = equipeVide;
				if (this.existgagnant)
					this.gagant = this.equipeAttaquee;

			}
		}

		/*
		 * condition atteindre un nbre de tour sans etre détruit(apres un nbre
		 * de tour l'equipe possedee au moins un jour
		 */
		// remarque : condition de victoir pour toute les equipes
		if ((nbrToursEffectues == nbrTourPourGagner)
				&& (listEquipes.get(equipeJoeu).getListRobots().size() != 0)) {
			existgagnant = true;
			this.gagant = "Nobre tours max atteint ";
		}
		return existgagnant;

	}

	public void PositionerEquipeCarte(String key) {
		/*
		 * Positionne les rebots d'une equipe sur une zone de depart
		 * 
		 * @param : cles d'acces a la liste de ses rebots dans la map
		 * listeEquipe, qui est aussi la cles d'acces a la zone de depart voulu
		 * dans la map des zonez de depart dans la carteJeux
		 */
		System.out.println();
		System.out.println("Positionnement de l'" + key);
		Iterator<Robot> it;
		Iterator<Position> ItPos;
		int index;
		Robot robotPositionne;
		// on manipule directement la vector sans clone(Elle existe toujours
		// dans le fichier text)

		// On met à jour le nom de la zone de depart
		ItPos = this.carteJeux.getZone(key).iterator();
		// Donner le nom de l'equipe a
		while (ItPos.hasNext()) {
			Case cse = carteJeux.getCase(ItPos.next());
            cse.setZoneDepart(key);
            cse.getPosition().getPositionX();
		}

		// on positione aleatoirement les robot dans la zone de depart
		System.out
				.println("Nom Robot               NomEquipe                     X               Y");
		it = listEquipes.get(key).getListRobots().iterator();
		while (it.hasNext()) {
			robotPositionne = it.next();
			index = (int) (Math.random() * (this.carteJeux.getZone(key).size() - 1)) + 0;
			robotPositionne.setPositionRobot(this.carteJeux.getZone(key).get(
					index));
			robotPositionne.setEquipe(key);
			carteJeux.PositionnerRobot(robotPositionne);
			logger.info(robotPositionne.getIdRobot() + "               "
					+ robotPositionne.getEquipe() + "                     "
					+ robotPositionne.getPositionRobot().getPositionX()
					+ "               "
					+ robotPositionne.getPositionRobot().getPositionY());
			this.carteJeux.getZone(key).remove(index);

		}

	}

	public Position atteindrePosition(Position position1, Position position2,
			int porteeArme) {

		/*
		 * @param: position rebot attaquant
		 * 
		 * @param: position de l'attaqué
		 * 
		 * @Return: revoi la position que le tire de rebot peut etteindre, si
		 * elle trouve un mur, rebot dans son chemin elle nous renvoi sa
		 * position, et si la porté de l'arme n'est pas suffisante, elle nous renvoi la position max qu'elle peut atteindre
		 * 
		*/

		Dir direct;
		int portee = porteeArme;
		Position position3 = position1;
		boolean embara = false;

		// Si le tire sur l'axe des x ou celui de Y=> deplacement permet
		// si sur la diagonale => non permet
		if ((position1.getPositionX() == position2.getPositionX())
				|| (position1.getPositionY() == position2.getPositionY())) {

			// Tire vers X ++
			if (position1.getPositionX() < position2.getPositionX())
				direct = Dir.B;
			else {
				// Tire vers X --
				if (position1.getPositionX() > position2.getPositionX())
					direct = Dir.H;
				else {
					// Tire vers Y++
					if (position1.getPositionY() < position2.getPositionY())
						direct = Dir.D;
					else { // Tire vers Y --
						if (position1.getPositionY() > position2.getPositionY())
							direct = Dir.G;
						else
							// Tire sur lui meme !
							direct = Dir.M;
					}
				}
			}
		} else
			// Tire non permet sur les diagonale
			direct = Dir.NP;

		switch (direct) {
		case B: {
			for (int i = position1.getPositionX() + 1; i <= position2
					.getPositionX(); i++) {
				if (this.pointActionReste > 0 && portee >0) {
					this.pointActionReste = this.pointActionReste - 1;
					portee -= 1;
					position3.setPositionX(i);
					
					if (!(carteJeux.getCase(position3).getContenu() == CaseType.VID)) {
						System.out.println("Deplacement ver le haut");
					}
				}
			}
		}
			break;

		case H: {
			for (int i = position1.getPositionX() - 1; i >= position2
					.getPositionX(); i--) {
				if (this.pointActionReste > 0 && portee >0 && !embara) {
					this.pointActionReste = this.pointActionReste - 1;
					portee -= 1;
					position3.setPositionX(i);
					if (!(carteJeux.getCase(position3).getContenu() == CaseType.VID)) {
						embara = true;
					}
				}
			}
		}
			break;
			
		case D: {
			for (int i = position1.getPositionY() + 1; i <= position2
					.getPositionY(); i++) {
				if (this.pointActionReste > 0 && portee >0 && !embara) {
					this.pointActionReste = this.pointActionReste - 1;
					portee -= 1;
					position3.setPositionY(i);
					mapDesEquipes.get(equipeJoeu).getMapRobotParcours()
							.get(robotJoeu.getIdRobot())[position3
							.getPositionX()][position3.getPositionY()] = true;

					if (!(carteJeux.getCase(position3).getContenu() == CaseType.VID)) {
						embara = true;

					}
				}
			}

		}
			break;

		case G: {
			for (int i = position1.getPositionY() - 1; i >= position2
					.getPositionY(); i--) {
				if (this.pointActionReste > 0 && portee >0 && !embara ) {
					this.pointActionReste = this.pointActionReste - 1;
					portee -= 1;
					position3.setPositionY(i);
					if (!(carteJeux.getCase(position3).getContenu() == CaseType.VID)) {
						embara = true;
					}
				}
			}
		}
			break;
		case M: {
			System.out.println("tire sur lui meme !!");
			this.pointActionReste--;
		}
			break;
		case NP: {
			logger.info("tire non permet sur diagonale !");
			this.pointActionReste = this.pointActionReste - 2;
		}
		}
		return position3;

	}

	public Position nouvellePosition(Position position1, Position position2) {

		/*
		 * Elle nous revoi la position que le rebot peut etteindre, s'il trouve
		 * un mur ou rebot dans son chemin elle nous renvoi sa position
		 */

		Dir direct;
		Position position3 = position1.clone();

		// Si deplacement et sur l'axe des x ou celui de Y=> deplacement pemet
		if ((position1.getPositionX() == position2.getPositionX())
				|| (position1.getPositionY() == position2.getPositionY())) {

			// Si le rebot veut se deplacer dans l'axe des X ++
			if (position1.getPositionX() < position2.getPositionX())
				direct = Dir.B;
			else {
				// Si le rebot veut se deplacer dans l'axe des X --
				if (position1.getPositionX() > position2.getPositionX())
					direct = Dir.H;
				else {
					// Si le rebot veut se deplacer dans l'axe des Y++
					if (position1.getPositionY() < position2.getPositionY())
						direct = Dir.D;
					else { // Si le rebot veut se deplacer dans l'axe des X --
						if (position1.getPositionY() > position2.getPositionY())
							direct = Dir.G;
						else
							// Meme position
							direct = Dir.M;
					}
				}
			}
		} else
			// deplcement non permet
			direct = Dir.NP;

		// Si le rebot veut se deplacer dans l'axe des X +
		System.out.println("*********************************" + direct);
		switch (direct) {
		case B: {
			for (int i = position1.getPositionX() + 1; i <= position2
					.getPositionX(); i++) {
				if (this.pointActionReste > 1) {
					this.pointActionReste = this.pointActionReste - 1;
					position3.setPositionX(i);
					/*
					 * mapDesEquipes.get(equipeJoeu).getMapRobotParcours()
					 * .get(robotJoeu.getIdRobot())[position3
					 * .getPositionX()][position3.getPositionY()] = true;
					 */
					if (!(carteJeux.getCase(position3).getContenu() == CaseType.VID)) {
						System.out.println("Deplacement ver le haut");
						return position3;
					}
				}
			}
		}
			break;

		case H: {
			for (int i = position1.getPositionX() - 1; i >= position2
					.getPositionX(); i--) {
				if (this.pointActionReste > 1) {
					this.pointActionReste--;
					position3.setPositionX(i);
					if (!(carteJeux.getCase(position3).getContenu() == CaseType.VID)) {
						System.out.println("deplacement vers le bas");
						return position3;
					}
				}
			}
		}
			break;

		// Si le rebot veut se deplacer dans l'axe des Y ++
		case D: {
			for (int i = position1.getPositionY() + 1; i <= position2
					.getPositionY(); i++) {
				if (pointActionReste > 1) {
					pointActionReste = this.pointActionReste - 1;
					position3.setPositionY(i);
					mapDesEquipes.get(equipeJoeu).getMapRobotParcours()
							.get(robotJoeu.getIdRobot())[position3
							.getPositionX()][position3.getPositionY()] = true;

					if (!(carteJeux.getCase(position3).getContenu() == CaseType.VID)) {
						System.out.println("Deplacement à droite");
						return position3;

					}
				}
			}

		}
			break;

		// Si le rebot veut se deplacer dans l'axe des Y --
		case G: {
			for (int i = position1.getPositionY() - 1; i >= position2
					.getPositionY(); i--) {
				if (this.pointActionReste > 1) {
					this.pointActionReste = this.pointActionReste - 1;
					position3.setPositionY(i);
					System.out.println(position3.getPositionX());
					System.out.println(position3.getPositionY());
					System.out.println(equipeJoeu);
					mapDesEquipes.get(equipeJoeu).getMapRobotParcours()
							.get(robotJoeu.getIdRobot());
					if (!(carteJeux.getCase(position3).getContenu() == CaseType.VID)) {
						logger.info("deplacement a gauche");
						return position3;
					}
				}
			}
		}
			break;
		case M: {
			System.out.println("vous rester dans la mm place !!");
			this.pointActionReste--;
			return position1;
		}
		case NP: {
			logger.info("deplacement non permet !");
			this.pointActionReste = this.pointActionReste - 2;
			return position1;
		}
		}
		return position3;
		
	}

	public void executeAction(Action action) {
		/*
		 * @param: tableau de trois cases, code de l'action a faire, et une
		 * position( la destination robot, ou la position robot adverse
		 * action[0]: code de l'action (action[1], action[2]): position
		 */
		if (action.getCodeAction().equals(CodeAction.DETECT)) { // code de la
																// detection
			detecter();
			logger.info("vous avez fait une detection");
		} else {
			if (action.getCodeAction().equals(CodeAction.DEPLACE)) {// code du
																	// deplacement

				// position destination de robot
				Position destination = action.getPosition();
				deplacer(destination);
				logger.info("vous avez fait un deplacement");
			} else {

				if (action.getCodeAction().equals(CodeAction.ATTAQUE)) {// code
																		// d'une
																		// attaque

					// position robot adverse
					Position positionRobAdverse = action.getPosition();
					attaquer(positionRobAdverse);
					logger.info("vous avez fait une attaque");

				} else {
					this.pointActionReste -= 8;
					System.out.println("vous avez perdu 8 point");

				}
			}
		}
	}

	public boolean existeRobot(String key, Robot robot) {
		boolean existe = false;
		int index = this.listEquipes.get(key).getListRobots().indexOf(robot);
		if (index != -1) {
			Robot r = this.listEquipes.get(key).getListRobots().get(index);
			if (carteJeux.getCase(r.getPositionRobot()).getIdRobot()
					.equals(robot.getIdRobot()))
				existe = true;

		}
		return existe;

	}
	
	
	public void lancer(){
		while (!existgagnant) {
			/*
			 * Faire un nouveau tour
			 */

			this.nbrToursEffectues += 1;
			logger.info("Tour numero" + nbrToursEffectues);
			for (String keyEquipe : listEquipes.getKeySet()) {
				System.out.println(keyEquipe);
			}

			for (String keyEquipe : listEquipes.getKeySet()) {
				//pour toutes les equipes
				
				logger.info("l'" + keyEquipe + " joue");
				this.pointActionReste = this.pointsAction;
				this.equipeJoeu = keyEquipe;
				if (!existgagnant) {
					Robot robot = listEquipes.get(keyEquipe).choixRobotJoue();
					if (existeRobot(keyEquipe, robot)) {
						this.robotJoeu = robot;
						logger.info("l'" + keyEquipe + " a choisi le rebot "
								+ this.robotJoeu.getIdRobot());

						while ((pointActionReste > 1) && !existgagnant
								&& robotJoeu != null) {

							action = listEquipes.get(keyEquipe).choixAction(
									robot);
							logger.info("le rebot position: "
									+ robotJoeu.getPositionRobot()
											.getPositionX()
									+ ", "
									+ robotJoeu.getPositionRobot()
											.getPositionY()
									+ " a choisi d'action "
									+ this.action.getCodeAction()
									+ "Position  "
									+ this.action.getPosition().getPositionX()
									+ "    "
									+ this.action.getPosition().getPositionY());

							this.executeAction(action);
							carteJeux.afficherCarte();
							this.testGagnant();

						}
					} else {
						// Si le robot n'existe pas dans l'euqipe
						logger.info("votre robot n'existe pas, ou il n'est pas bien placé!");
						logger.info("l'" + keyEquipe + " a perdu son tour");

					}

				}

			}

		}
		System.out.println();
		System.out.println();
		System.out.println("FIN DE JEUX");
		System.out.println(existgagnant);
		System.out.println("L'equipe " + this.gagant + "a gagné BRAVO");

	}


	public JeuGraphique() throws InstantiationException, IllegalAccessException,
			IOException {
		this.existgagnant = false;
		this.gagant = "";
		this.nbrToursEffectues = 0;
		this.action = new Action();
		this.robotJoeu = new Robot();
		this.listEquipes = new ConcurrentMultiMap<String, Equipe>();
		this.mapDesEquipes = new ConcurrentMultiMap<String, ParcourtCarte>();
		//this.fenetre = new InterfaceGraphique();
		
		logger.info("Vous êtes au debut");
		Scanner sc = new Scanner(System.in);

		// chargement de Senario
		logger.info("Choix de senario");
		// chargementSenario(numerosenario);
		chargementSenario(fenetre.liste.getItemCount());
		this.nbrEquipes = carteJeux.getNbrZones();
		for (int i = 1; i <= nbrEquipes; i++) {
			
		/*	logger.info("Chargemment equipe " + i);
			String nonEquipe = "equipe" + i;
			ChargerEquipe(fenetre.cheminJar.getText(), nonEquipe);
			PositionerEquipeCarte(nonEquipe);*/
		}

		System.out.println("dedut de jeux" + listEquipes.size());
		System.out.println("                            carte                            ");
		carteJeux.afficherCarte();
		System.out.println("                            /carte                            ");
		
		
		System.out.println();
		System.out.println();
		System.out.println("FIN DE JEUX");
		System.out.println(existgagnant);
		System.out.println("L'equipe " + this.gagant + "a gagné BRAVO");
		
	}

	public static void main(String[] args) throws InstantiationException,
		IllegalAccessException, IOException {
		JeuGraphique jeu = new JeuGraphique();
		InterfaceGraphique fenetre = new InterfaceGraphique();
		fenetre.jeuG= jeu;
		jeu.fenetre = fenetre;
		
		
	}
	public void ChargerEquipeG(String URL, String nomEquipe)
			throws InstantiationException, IllegalAccessException {

		Equipe equipe = null;
		try {
			File file = new File(URL);

			String classToLoad = nomEquipe;

			URL jarUrl = new URL("jar", "", "file:" + file.getAbsolutePath()
					+ "!/");
			URLClassLoader cl = new URLClassLoader(new URL[] { jarUrl },
					Jeux.class.getClassLoader());
			Class<?> loadedClass = cl.loadClass(classToLoad);
			equipe = (Equipe) loadedClass.newInstance();
			System.out.println(equipe.getNomEquipe());
			listEquipes.put(nomEquipe, equipe);

			ParcourtCarte parcourtCarte = new ParcourtCarte();

			Iterator<Robot> iter = equipe.getListRobots().iterator();
			while (iter.hasNext()) {
				parcourtCarte.ajouterRobot(iter.next().getIdRobot(),
						this.carteJeux.getCarteParcourt().clone());
			}
			this.mapDesEquipes.put(nomEquipe, parcourtCarte);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}
}