package moteurJeux;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.ItemSelectable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.FeatureDescriptor;
import java.io.File;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.table.AbstractTableModel;

import creationEquipe.CaseType;
import creationEquipe.Position;

import cartes.Carte;

public class InterfaceGraphique extends JFrame {
	//elle contient main on peu la tester
    JTextArea journal;
    JTable tableau;//affichage de la carte
    JComboBox liste;//selection de carte
    JPanel paneau;
    JButton bouton1, bouton2, jb_browse;
    JTextField cheminJar, nomClasse;
    JeuGraphique jeuG;
    MonModele model;

    public InterfaceGraphique() {
      
    	setLayout(new GridBagLayout());
        cheminJar = new JTextField(40);
        nomClasse = new JTextField(30);
        jb_browse = new JButton("Chercher Equipe...");
        jb_browse.addActionListener(new EvenButon());  
        add(cheminJar, constraint(0, 1, 1, 1));
        add(jb_browse, constraint(2, 1, 1, 1));	
        add(nomClasse, constraint(3, 1, 1, 1));
        bouton1 = new JButton("Ajouter Equipe");
        add(bouton1, constraint(4, 1, 1, 1));
        bouton2 = new JButton("Lancer Jeu");
        add(bouton2, constraint(3, 4, 1, 1));
        bouton2.addActionListener(new EvenButon());       
        tableau = new JTable();
        liste = new JComboBox();
        liste.addItemListener(new  ItemListener(){
			public void itemStateChanged(ItemEvent e) {
				if(jeuG.getLongListeEquipe() == 0)
					jeuG.chargementSenario(liste.getSelectedIndex());				
			}        	
        });
      //  liste.addItem("carte 1");
      //  liste.addItem("carte 2");
        add(liste, this.constraint(1, 0, 1, 1));
      //juste pour voir la forma de l interface
       // tableau.setPreferredScrollableViewportSize(new Dimension(400, 400));
        //tableau.setFillsViewportHeight(true);
        JScrollPane scrollpane = new JScrollPane(tableau);
        add(scrollpane, constraint(0, 2, 2, 1));
        journal = new JTextArea(20, 50);
        JScrollPane scrollpane1 = new JScrollPane(journal);
        add(scrollpane1, constraint(2, 2, 2, 1));
        
    }
    private GridBagConstraints constraint(int a, int z, int e, int r) {
        GridBagConstraints constr = new GridBagConstraints();
        constr.gridx = a;
        constr.gridy = z;
        constr.gridwidth = e;
        constr.gridheight = r;
        constr.insets = new Insets(10, 10, 10, 10);
        return constr;
    }
   
    public static void main(String[] args) {
        InterfaceGraphique fenetre = new InterfaceGraphique();
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fenetre.setSize(1400, 700);
        fenetre.setVisible(true);
        fenetre.setTitle("my first java table");
       
    }
    private void bordertout(JComponent a) {

        Border thickBorde = new LineBorder((a.getParent()).getBackground(), 12);
        a.setBorder(thickBorde);
    }
    class EvenButon implements ActionListener{
    	public EvenButon() {
    		super();
    	}
    	 public void actionPerformed(ActionEvent arg0) {
     		JFileChooser chooser = new JFileChooser();
     		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
      
             if (chooser.showOpenDialog(jb_browse.getParent()) == JFileChooser.APPROVE_OPTION) {
                 File file = chooser.getSelectedFile();
                 cheminJar.setText(file.getPath());
             }             	
     	}
    } 
    class EventAjoutEquipe implements ActionListener{
    	public EventAjoutEquipe() {
    		super();
    	}
    	 public void actionPerformed(ActionEvent arg0) {
    		 if(cheminJar.getText()!="" && nomClasse.getText() != "" && jeuG.getLongListeEquipe()<jeuG.getCarteJeux().getNbrZones()){
    			 try {
					jeuG.ChargerEquipeG(cheminJar.getText(), nomClasse.getText());
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
    		 }else 
    				cheminJar.setText("saisir le nom et le chemin") ;
    		 }
     	}
    
    class EventLancerJeu implements ActionListener{
    	public EventLancerJeu() {
    		super();
    	}
    	 public void actionPerformed(ActionEvent arg0) {
    		 if( jeuG.getLongListeEquipe()>0){
    			 bouton2.setEnabled(true);
    			 MonModele modele = new MonModele(jeuG.getCarteJeux());
    			 tableau.setModel(modele);
    			jeuG.lancer(); 
    			 bouton2.setEnabled(false);
    		 }else 
    				cheminJar.setText("saisir le nom et le chemin") ;
    		 }
     	}
    } 



 
class MonModele extends AbstractTableModel{ 
	Carte carte;

	   public MonModele(Carte carte){this.carte = carte; }//this.carte = new CarteJeux();} 
	   public Object getValueAt(int i, int j){
	    		  Position p = new Position(i, j);	    		 
	    			  return carte.getCase(p).getSymbole();  	    		 
	   }	
	public int getColumnCount() {
		return carte.getDimensionX();
	}
	public int getRowCount() {
		return carte.getDimensionY();
	}
} 

