/*
 * class SerialiserCartes
 *
 * 1.0
 *
 * 24/10/2012
 *
 * copyright: M2 ACSIS 2012/2013
 */

package cartes;

import java.io.*;
import java.util.ArrayList;
/**
 * Serialise les strategies dans un fichiers text
 */
public class SerialiserCartes implements Serializable {
	
	/**
	 * une liste qui contient toute les cartes
	 */
	private ArrayList<Carte> listCartes;

	public SerialiserCartes() {
		listCartes = new ArrayList<Carte>();

	}

	/**
	 * ceation d'une carte et l'enregister dans un fichie texte
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void serialiser() throws FileNotFoundException, IOException {

		ObjectOutputStream fluxSerialisation = new ObjectOutputStream(
				new BufferedOutputStream(new FileOutputStream(new File(
						"Cartes.txt"))));

		fluxSerialisation.writeObject(this.listCartes);
		
		fluxSerialisation.close();

	}

	/**
	 * ajouter une carte
	 * @param carte
	 */
	public void addCarte(Carte carte) {
		this.listCartes.add(carte);
	}

	public ArrayList<Carte> getListCartes() {
		return listCartes;
	}

	/**
	 * remplir une carte a partir d'un fichier texte
	 * @return liste des cartes 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public ArrayList<Carte> dessirialiserCartes() throws FileNotFoundException,
			IOException, ClassNotFoundException {
		ObjectInputStream fluxDeserialisation = new ObjectInputStream(
				new BufferedInputStream(new FileInputStream(new File(
						"Cartes.txt"))));

		this.listCartes = (ArrayList<Carte>) fluxDeserialisation.readObject();

		fluxDeserialisation.close();
		
		return this.listCartes;
	}


}
