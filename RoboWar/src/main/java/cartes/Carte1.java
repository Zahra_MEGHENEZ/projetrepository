package cartes;

import creationEquipe.*;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * definit une carte appele carte1
 *
 */
/**
 * @author rima
 *
 */
/**
 * @author rima
 *
 */
public class Carte1 extends Carte {

	/**
	 * initialise la carte1 
	 * c'est une carte de 20 lignes et 20 colonnes avec 2 zones de depart
	 * @param nomCarte
	 */
	public Carte1(String nomCarte) {
		// Initialisation de variales
		this.zonesDepart = new HashMap<String, ArrayList<Position>>();
		this.conditionsVectoire = new HashMap<String, boolean[]>();
		this.carteParcourt = new boolean[20][20];
		this.carte = new Case[20][20];

		this.nomCarte = nomCarte;
		this.setDimensionX(20);
		this.setDimensionY(20);
		this.nbrZones = 2;
        
		// Creation de senario
		createCarte();
		initCarteParcourt();
		setConditionsVectoire();

	}


	protected void createCarte() {

		this.carte = new Case[20][20];

		for (int i = 0; i < this.getDimensionX(); i++) {
			for (int j = 0; j < this.getDimensionY(); j++) {
				this.carte[i][j] = new Case(new Position(i,j));

			}
		}
		// placer les murs autour de la carte
		for (int i = 0; i < this.getDimensionY(); i++) {
			this.carte[0][i].setContenu(CaseType.MUR);
			this.carte[0][i].setSymbole("#");
			this.carte[this.getDimensionX() - 1][i].setContenu(CaseType.MUR);
			this.carte[this.getDimensionX() - 1][i].setSymbole("#");
		}
		for (int i = 1; i < this.getDimensionX() - 1; i++) {
			this.carte[i][0].setContenu(CaseType.MUR);
			this.carte[i][0].setSymbole("#");
			this.carte[i][19].setContenu(CaseType.MUR);
			this.carte[i][19].setSymbole("#");
		}
        
		
		
		// placer les murs aléatoirement à l'intérieur de la carte
		for (int i = 0; i < 20; i++) {
			int a = (int) (Math.round(Math.random() * 18) + 1);
			int b = (int) (Math.round(Math.random() * 18) + 1);

			this.carte[a][b].setContenu(CaseType.MUR);
			this.carte[a][b].setSymbole("#");
		}

		// la zone de départ equipe1
		ArrayList<Position> zoneDepEquipe1 = new ArrayList<Position>();

		for (int i = 1; i < 4; i++) {
			for (int j = 1; j < 5; j++) {
				this.carte[i][j].setContenu(CaseType.VID);
				this.carte[i][j].setSymbole("*");
				this.carte[i][j].setZoneDepart("equipe1");
				zoneDepEquipe1.add(new Position(i, j));
			}
		}

		// draeau
		 // placer un drapeau
		
		
		this.carte[1][1].setContenu(CaseType.DRAPEAU);
		this.carte[1][1].setSymbole("D");
		this.carte[1][1].setNomEquipe("equipe1");
		zoneDepEquipe1.remove(new Position (1,1));
		this.zonesDepart.put("equipe1", zoneDepEquipe1);

		
		
		
		// la zone de départ equipe2

		ArrayList<Position> zoneDepEquipe2 = new ArrayList<Position>();
		for (int i = 16; i < 19; i++) {
			for (int j = 15; j < 19; j++) {
				this.carte[i][j].setContenu(CaseType.VID);
				this.carte[i][j].setSymbole("*");
				this.carte[i][j].setZoneDepart("equipe2");
				zoneDepEquipe2.add(new Position(i, j));
			}
		}
		this.carte[18][18].setContenu(CaseType.DRAPEAU);
		this.carte[18][18].setSymbole("D");
		this.carte[18][18].setNomEquipe("equipe2");
		zoneDepEquipe1.remove(new Position (18,18));
		
		this.zonesDepart.put("equipe2", zoneDepEquipe2);

	}

	private void setConditionsVectoire() {
		boolean[] conditionsVictoire = { false, true, false, false };
		this.conditionsVectoire.put("equipe1", conditionsVictoire);
		this.conditionsVectoire.put("equipe2", conditionsVictoire);
	}

	@Override
	public void PositionnerRobot(Robot robot) {
		int x = robot.getPositionRobot().getPositionX();
		int y = robot.getPositionRobot().getPositionY();
		this.getCarte()[x][y].setContenu(CaseType.ROB);
		this.getCarte()[x][y].setIdRobot(robot.getIdRobot());
		this.getCarte()[x][y].setNomEquipe(robot.getEquipe());
		this.getCarte()[x][y].setPosition(robot.getPositionRobot());
		if(robot.getEquipe().equals("equipe1")){
			this.getCarte()[x][y].setSymbole("&");
		}
		else
			this.getCarte()[x][y].setSymbole("~");
 
		
		
	}

}
