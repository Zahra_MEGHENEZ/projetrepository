package cartes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import creationEquipe.Case;
import creationEquipe.CaseType;
import creationEquipe.Position;
import creationEquipe.Robot;

/**
 * définit la struccture d'un sénario de jeux 

 *
 */
public abstract class Carte implements Serializable {
	

	/**
	 * le non de la carte sur la quelle je jeux sera dérouleé
	 */
	String nomCarte;
	/**
	 * le nombre de zones de depart dans la carte
	 */
	protected int nbrZones;
	/**
	 * la carte est une matrice de type case
	 */
	protected Case[][] carte;
	/**
	 * le nombre de ligne
	 */
	protected int dimensionX;
	/**
	 * le nombre de colonne
	 */
	protected int dimensionY;
	/**
	 * matrice pour sauvgarder les traces des robot des équipes
	 */
	protected boolean[][] carteParcourt;
	
	
	
	/**
	 * postions de chaque zone de depart
	 */
	protected HashMap<String, ArrayList<Position>> zonesDepart;

	
	/**
	 *  chaque ligne correspond au represente les conditions de vectoire d'une equipe
	 */
	protected HashMap<String, boolean[]> conditionsVectoire =new HashMap<String, boolean[]>(); //drapeu, detruire, parcourir, nbre tour
	

	public boolean[][] getCarteParcourt() {
		return this.carteParcourt;
	}

	public void setCarteParcourt(boolean[][] carteParcourt) {
		this.carteParcourt = carteParcourt;
	}

	public String getNom() {
		return nomCarte;
	}

	public int getNbrZones() {
		return nbrZones;
	}

	public Case[][] getCarte() {
		return carte;
	}

	public int getDimensionX() {
		return dimensionX;
	}
	public void setDimensionX(int dimensionX){
		this.dimensionX=dimensionX;
	}

	public int getDimensionY() {
		return dimensionY;
	}
	public void setDimensionY(int dimensionY){
		this.dimensionY=dimensionY;
	}
	
	public Case getCase(Position position){
		return this.getCarte()[position.getPositionX()][position.getPositionY()];	
	}
	/**
	 * vider une case 
	 * @param position a vider 
	 */
	public void viderCase(Position position){
		int x = position.getPositionX();
		int y = position.getPositionY();
		System.out.println(x+"    "+y);
		this.getCarte()[x][y].setContenu(CaseType.VID);
		this.getCarte()[x][y].setNomEquipe(null);
		this.getCarte()[x][y].setSymbole("-");
		
	}
	
	public HashMap<String, boolean[]> getConditionsVectoire() {
		return conditionsVectoire;
	}

	/**
	 * positionner un robot dans la carte
	 * @param robot
	 */
	public abstract void PositionnerRobot(Robot robot) ;
	public ArrayList<Position> getZone(String keyZone){
		return this.zonesDepart.get(keyZone);
	}
	
	
	
	protected abstract void createCarte();
	/**
	 * creation d'une carte
	 */

	protected void initCarteParcourt() {
		/**
		 * initialiser la carteparcourt
		 */
		for (int i = 0; i < this.getDimensionX(); i++) {
			for (int j = 0; j < this.getDimensionY(); j++) {
				if (carte[i][j].equals(CaseType.MUR))
					this.carteParcourt[i][j] = true;
				else
					this.carteParcourt[i][j] = false;
			}
		}

	}
	
	public void afficherCarte() {
		for (int i = 0; i < this.getDimensionX(); i++) {
			for (int j = 0; j < this.getDimensionY(); j++)  {
				System.out.print(carte[i][j].getSymbole() + " ");
			}
				
			System.out.println();
		}
	}

	
	
	

}
