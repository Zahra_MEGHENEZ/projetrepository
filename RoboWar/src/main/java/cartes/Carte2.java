package cartes;

import creationEquipe.*;

import java.util.ArrayList;
import java.util.HashMap;

public class Carte2 extends Carte {

	public Carte2(String nomCarte) {
		// Initialisation de variales
		this.zonesDepart = new HashMap<String, ArrayList<Position>>();
		this.conditionsVectoire = new HashMap<String, boolean[]>();
		this.carteParcourt = new boolean[20][20];
		this.carte = new Case[10][12];

		this.nomCarte = nomCarte;
		this.setDimensionX(10);
		this.setDimensionY(12);
		this.nbrZones = 2;
        
		// Creation de senario
		createCarte();
		initCarteParcourt();
		setConditionsVectoire();

	}

	// méthode pour l'initialisation du parcours de la carte


	@Override
	protected void createCarte() {

		this.carte = new Case[10][12];

		for (int i = 0; i < this.getDimensionX(); i++) {
			for (int j = 0; j < this.getDimensionY(); j++) {
				this.carte[i][j] = new Case(new Position(i,j));

			}
		}
		// placer les murs autour de la carte
		for (int i = 0; i < this.getDimensionY(); i++) {
			this.carte[0][i].setContenu(CaseType.MUR);
			this.carte[0][i].setSymbole("#");
			this.carte[this.getDimensionX() - 1][i].setContenu(CaseType.MUR);
			this.carte[this.getDimensionX() - 1][i].setSymbole("#");
		}
		for (int i = 1; i < this.getDimensionX() - 1; i++) {
			this.carte[i][0].setContenu(CaseType.MUR);
			this.carte[i][0].setSymbole("#");
			this.carte[i][11].setContenu(CaseType.MUR);
			this.carte[i][11].setSymbole("#");
		}
        
		
		
		// placer les murs aléatoirement à l'intérieur de la carte
		for (int i = 0; i < 8; i++) {
			int a = (int) (Math.round(Math.random() * 8) + 1);
			int b = (int) (Math.round(Math.random() * 10) + 1);

			this.carte[a][b].setContenu(CaseType.MUR);
			this.carte[a][b].setSymbole("#");
		}

		// la zone de départ equipe1
		ArrayList<Position> zoneDepEquipe1 = new ArrayList<Position>();

		for (int i = 1; i < 4; i++) {
			for (int j = 1; j < 4; j++) {
				this.carte[i][j].setContenu(CaseType.VID);
				this.carte[i][j].setSymbole("*");
				this.carte[i][j].setZoneDepart("equipe1");
				zoneDepEquipe1.add(new Position(i, j));
			}
		}

	
		 // placer un drapeau
		this.carte[1][1].setContenu(CaseType.DRAPEAU);
		this.carte[1][1].setSymbole("D");
		this.carte[1][1].setNomEquipe("equipe1");
		zoneDepEquipe1.remove(new Position (1,1));
		this.zonesDepart.put("equipe1", zoneDepEquipe1);

		
		
		
		// la zone de départ equipe2

		ArrayList<Position> zoneDepEquipe2 = new ArrayList<Position>();
		for (int i = 6; i < 9; i++) {
			for (int j = 8; j < 11; j++) {
				this.carte[i][j].setContenu(CaseType.VID);
				this.carte[i][j].setSymbole("*");
				this.carte[i][j].setZoneDepart("equipe2");
				zoneDepEquipe2.add(new Position(i, j));
			}
		}
		this.carte[8][10].setContenu(CaseType.DRAPEAU);
		this.carte[8][10].setSymbole("D");
		this.carte[8][10].setNomEquipe("equipe2");
		zoneDepEquipe1.remove(new Position (18,18));
		
		this.zonesDepart.put("equipe2", zoneDepEquipe2);

	}

	private void setConditionsVectoire() {
		boolean[] conditionsVictoire1 = { true, true, false, false };
		this.conditionsVectoire.put("equipe1", conditionsVictoire1);
		boolean[] conditionsVictoire2 = { true, true, false, false };
		this.conditionsVectoire.put("equipe2", conditionsVictoire2);
	}

	@Override
	public void PositionnerRobot(Robot robot) {
		int x = robot.getPositionRobot().getPositionX();
		int y = robot.getPositionRobot().getPositionY();
		this.getCarte()[x][y].setContenu(CaseType.ROB);
		this.getCarte()[x][y].setIdRobot(robot.getIdRobot());
		this.getCarte()[x][y].setNomEquipe(robot.getEquipe());
		this.getCarte()[x][y].setPosition(robot.getPositionRobot());
		if(robot.getEquipe().equals("equipe1")){
			this.getCarte()[x][y].setSymbole("&");
		}
		else
			this.getCarte()[x][y].setSymbole("~");
 
		
		
	}
	public static void main(String[] args) {
		Carte2  c2 = new Carte2("carte2");
		c2.afficherCarte();
		
	}



}
