package cartes;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Creation des sénarios (carte+condition de victoire) 
 *
 */
public class CreationDesCartes {

	/**
	 * Creation des cartes qui seront utilisées dans le jeux
	 */

	public CreationDesCartes() throws FileNotFoundException, IOException {

		SerialiserCartes serialse = new SerialiserCartes();
        Carte1  carte1 = new Carte1("Carte1");
        serialse.addCarte(carte1);
        
        Carte2  carte2 = new Carte2("Carte2");
        serialse.addCarte(carte2);
        
		serialse.serialiser();
		
	}
	/**
	 * lancer la creation
	 * @param args
	 */
	public static void main(String[] args){
		try {
			new  CreationDesCartes();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
